Archiwum archive.zip zawiera archiwum plików i kodów opracowanych podczas tworzenia systemu rekomendacji fimów oraz plik "README wewn.txt" opisujący ich otwieranie.

Aby mieć dostęp do archiwum należy je pobrać na komputer oraz wypakować.

Wypakowywanie pliku .zip odbywa się poprzez kliknięcie na niego prawym przyciskiem myszy oraz wybranie opcji "wyodrębnij wszystkie".

Aby uruchomić opracowaną aplikację należy postąpić zgodnie z instrukcją zawartą w pliku "README wewn.txt".